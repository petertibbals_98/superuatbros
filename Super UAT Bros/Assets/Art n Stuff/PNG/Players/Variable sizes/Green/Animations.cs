﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    public static AnimatorController instance;

    Transform tf;
    Animator myAnim;
    Vector3 artScaleCache;

    void Start()
    {
        tf = this.transform;
        myAnim = this.gameObject.GetComponent<Animator>();
        instance = this;

        artScaleCache = tf.localScale;
    }

    void FlipArt(float currentSpeed)
    {
        if ((currentSpeed < 0 && artScaleCache.x > 0) || //going left AND faceing right OR...
         (currentSpeed > 0 && artScaleCache.x < 0)) //going right AND facing left
        {
            //flip the art
            artScaleCache.x *= -1;
            tf.localScale = artScaleCache;
        }

    }

    public void UpdateSpeed(float currentSpeed)
    {
        myAnim.SetFloat("Speed", currentSpeed);
        FlipArt(currentSpeed);
    }

    public void UpdateIsGrounded(bool Grounded)
    {
        myAnim.SetBool("isGrounded", Grounded);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is used in another scipt so that I can pass a vector 3 into a vector 2
/// </summary>

public static class Extention
{
    public static Vector2 toVector2(this Vector3 vec3)
    {
        return new Vector2(vec3.x, vec3.y);
    }
}

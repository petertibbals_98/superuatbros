﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject projectile;  //the bullet
    public Vector2 velocity;
    bool canShoot = true;           //the bool used to make the player be able to shoot
    public Vector2 offset = new Vector2(0.4f, 0.1f);
    public float cooldown = 1f;     //the cd for the shooting
    public float DeathTimer = 1f;   //the cd for the bullet timer


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space) && canShoot) //makes him shoot
        {

            GameObject go = (GameObject)Instantiate(projectile, (Vector2)transform.position + offset * transform.localScale.x, Quaternion.identity);  //Makes the bullet

            go.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity.x * transform.localScale.x, velocity.y);

            StartCoroutine(CanShoot());  //starts coroutine so the player cannot shoot until a timer is up
            StartCoroutine(Death());     //Starts coroutine to kill the bullet after an amount of time
            

            //GetComponent<Animator>().SetTrigger("shoot");  To much to make another animation

        }


    }


    IEnumerator CanShoot() //Makes it so the player cannot shoot forever
    {
        canShoot = false;
        yield return new WaitForSeconds(cooldown);
        canShoot = true;


    }

    IEnumerator Death()  //Kills the bullet after a certain amount of time
    {
        yield return new WaitForSeconds(DeathTimer);
        Destroy(GameObject.FindWithTag("Bullet"));
    }
}

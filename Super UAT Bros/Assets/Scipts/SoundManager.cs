﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource audio;
    public AudioClip clip1;
    public AudioClip clip2;

	// Use this for initialization
	void Start ()
    {
        audio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.A)) //Playing audio in plcees. No real need for play clip at point in my opinion
        {
            //GetComponent<SpriteRenderer>().flipX = true;
            audio.clip = clip1;
            audio.Play();
           // GetComponent<Animator>().Play("Player_Walk");
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            //GetComponent<SpriteRenderer>().flipX = false;
            audio.clip = clip1;
            audio.Play();
          //  GetComponent<Animator>().Play("Player_Walk");
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            audio.clip = clip2;
            audio.Play();
          //  GetComponent<Animator>().Play("Player_Jump");
        }
    }
}

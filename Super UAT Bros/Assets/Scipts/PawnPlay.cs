﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnPlay : Pawn
{

    public Transform tf;     //the transform
    public Rigidbody2D rb;   //the rb
    public AnimatorController Anim;  //the animation contorller
    public Vector3 RespawnPoint;    //the respawn point
    public Leveling LvlManager;     

    public bool grounded;  //checks to see if player is on ground. Basically a reset check.
    public bool canDoubleJump;  //can player jump again
    public bool doneJumping;  //Isnt used but to lazy to get rid of

    public GameObject Victory;


    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();  //Setting lots of junk for things and readson
        rb = GetComponent<Rigidbody2D>();
        Anim = AnimatorController.instance; //Setting things to values
        RespawnPoint = transform.position;
        LvlManager = FindObjectOfType<Leveling>();
        Victory.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       //Sorry Im pretty sarcastic in my code
        if (rb.velocity.x > 0.00f)
        {
            GetComponent<Animator>().SetFloat("Speed", moveSpeed);
            GetComponent<Animator>().Play("Player_Walk");
        }

        else if (rb.velocity.y > 0.01f)  //plays animations if volocity is under 
        {
            GetComponent<Animator>().Play("Player_Jump");
        }
        else
        {
            GetComponent<Animator>().Play("Player_Idle");
        }

        /*
        if (Input.GetKeyDown(KeyCode.Space)) //Shooting, yay fun
        {
            GameObject bullet = Instantiate<GameObject>(GameManager.instance.bullet);
            bullet.transform.position = transform.position;
        }
        */

    }
    //I know what it should do, pretty simple stuff and if I think that then you shouldn't have an issue reading it
    public override void Move(float direction)
    {
        tf.position += Vector3.right * direction * moveSpeed; //Overriding the base move code
    }

    public override void Jump()
    {
        if (grounded) //checks and sees if player is grounded
        {
            rb.AddForce(Vector2.up * jumpForce);
            canDoubleJump = true;
            grounded = false;
            GetComponent<Animator>().Play("Player_Jump");
        }

        else if (canDoubleJump) //Lets them jump a second time
        {
            canDoubleJump = false;  //sets the bool to false so they cannot jump again
            rb.velocity = new Vector2(rb.velocity.x, 0);
            rb.AddForce(Vector2.up * jumpForce);
            GetComponent<Animator>().Play("Player_Jump");
        }

        else
        {
            grounded = true;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Fall Box") //Gonna kill them if they fall, kills player if they touch the fall box
        {
            LvlManager.Respawn();
        }

        if (other.tag == "Checkpoint") //Sets point for respawn, sets respawn to be the checlpoint location
        {
            RespawnPoint = other.transform.position;
        }

        if (other.tag == "Winning") //Checking for win con, winning the game if you colide with the desired obj
        {
            Victory.gameObject.SetActive(true);
            Debug.Log("Hey there");
            Time.timeScale = 0;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CantKillMusic : MonoBehaviour {

	void Awake ()
    {
        DontDestroyOnLoad(transform.gameObject);  //Prevents the music component from being destroyed
	}
	
}

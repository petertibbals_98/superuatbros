﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This code is used to set checkpoints based on when the player to a set point  
/// </summary>

public class Checkpoint : MonoBehaviour

{
    public Sprite OffFlag; //sprites for the checkpoints 
    public Sprite OnFlag;
    private SpriteRenderer sp;
    public bool checkpointReached; //What checkpoint you are on
    
    // Use this for initialization
    void Start()
    {
        sp = GetComponent<SpriteRenderer>();  //sprite rendereer
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D other)  //sets the checkpoints when player coliders with it
    {
        if (other.tag == "Player") //Yea not much to say for this
        {
            sp.sprite = OnFlag;
            checkpointReached = true;
        }
    }
}
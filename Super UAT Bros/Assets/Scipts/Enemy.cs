﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    /// <summary>
    /// This code is used for the enemy. Plays their animations and whatnot along with killing the player and 
    /// getting destroyed themselves when attacked
    /// </summary>
    
    public LayerMask enemyMask; 
    public float speed = 1;  //FLoat for the speed
    Rigidbody2D myBody;  //USed for the rb
    Transform myTrans;
    float myWidth, myHeight; 
    public Leveling LvlManager;

    void Start()
    {
        myTrans = this.transform;
        myBody = this.GetComponent<Rigidbody2D>();
        SpriteRenderer mySprite = this.GetComponent<SpriteRenderer>();
        myWidth = mySprite.bounds.extents.x;
        myHeight = mySprite.bounds.extents.y;

        LvlManager = FindObjectOfType<Leveling>();
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Player")
        {
           // Debug.Log("Hello");
            Destroy(col.gameObject);
        }

    }

    void OnTriggerEnter2D(Collider2D other) //checking for things
    {
        if (other.gameObject.tag == "Player") //Kills astroid if they touch a bullet
        {

            // Also destroy bullet
            //Destroy(other.gameObject);
            LvlManager.Respawn();

        }

        if (other.gameObject.tag == "Bullet") //Kills astroid if they touch a bullet
        {
            Destroy(this.gameObject);
            
            // Also destroy bullet
            Destroy(other.gameObject);
        }
    }

    /*  Gave up on trying to make it move the way I wanted to
    void FixedUpdate()
    {

        //Use this position to cast the isGrounded/isBlocked lines from
        Vector2 lineCastPos = myTrans.position.toVector2() - myTrans.right.toVector2() * myWidth + Vector2.up * myHeight;
        //Check to see if there's ground in front of us before moving forward
        Debug.DrawLine(lineCastPos, lineCastPos + Vector2.down);
        bool isGrounded = Physics2D.Linecast(lineCastPos, lineCastPos + Vector2.down, enemyMask);
        //Check to see if there's a wall in front of us before moving forward
        Debug.DrawLine(lineCastPos, lineCastPos - myTrans.right.toVector2() * .05f);
        bool isBlocked = Physics2D.Linecast(lineCastPos, lineCastPos - myTrans.right.toVector2() * .05f, enemyMask);

        //If theres no ground, turn around. Or if I hit a wall, turn around
        if (!isGrounded || isBlocked)
        {
            Vector3 currRot = myTrans.eulerAngles;
            currRot.y += 180;
            myTrans.eulerAngles = currRot;
        }

        //Always move forward
        Vector2 myVel = myBody.velocity;
        myVel.x = -myTrans.right.x * speed;
        myBody.velocity = myVel;
    }
    */
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow_Player : MonoBehaviour
{
    private Vector2 velocity;
    public float smoothTimeY;  //Variable for time smoothing X and Y
    public float smoothTimeX;

    public GameObject Player;  //the player

    void start ()
    {
        Player = GameObject.FindGameObjectWithTag("Player");  //grabs the player

    }

    void FixedUpdate()  //Slowly moves the camera 
    {
        float posX = Mathf.SmoothDamp(transform.position.x, Player.transform.position.x, ref velocity.x, smoothTimeX);
        float posY = Mathf.SmoothDamp(transform.position.y, Player.transform.position.y, ref velocity.y, smoothTimeY);

        transform.position = new Vector3(posX, posY, transform.position.z); //moves the camera based on the player
    }
}
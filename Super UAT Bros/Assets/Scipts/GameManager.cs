﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public GameObject Heart_1;  //Varibles for the players health
    public GameObject Heart_2;
    public GameObject Heart_3;
    public GameObject GameOver;
    public GameObject Victory;
    public static int Health;  //Int used to show the health 


    // Bullet prefab
    public GameObject bullet;  //the bullet
    public float bulletSpeed; 
    public float bulletLife;

    public GameObject Player;

    void Awake()   //called when the thing is created
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    // Use this for initialization
    void Start ()
    {
        //Yep these are all things
        Health = 3;
        Heart_1.gameObject.SetActive(true);
        Heart_2.gameObject.SetActive(true);
        Heart_3.gameObject.SetActive(true);
        GameOver.gameObject.SetActive(false);
        Victory.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (Health > 3) //Checking for the health total and then changing the numbe of hearts to fit that number so you cant have less than three
            Health = 3;

        switch (Health)  //Im aware how long and stupid this is
        {                //This changes the shown health on the scrren based on the health int
            case 3:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(true);
                Heart_3.gameObject.SetActive(true);
                break;

            case 2:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(true);
                Heart_3.gameObject.SetActive(false);
                break;

            case 1:
                Heart_1.gameObject.SetActive(true);
                Heart_2.gameObject.SetActive(false);
                Heart_3.gameObject.SetActive(false);
                break;

            case 0: ///Prob a way better way to do this!!
                Heart_1.gameObject.SetActive(false);
                Heart_2.gameObject.SetActive(false);
                Heart_3.gameObject.SetActive(false);
                Time.timeScale = 0;
                GameOver.gameObject.SetActive(true);
                break;
        }
    }
}

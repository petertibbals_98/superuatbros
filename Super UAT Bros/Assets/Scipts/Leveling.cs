﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leveling : MonoBehaviour
{
    public float respawnDelay;   //Delay before the player is brought back
    public PawnPlay gamePlayer;  
    public Text LivesTxt;


    // Use this for initialization
    void Start()
    {
        gamePlayer = FindObjectOfType<PawnPlay>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Respawn()
    {
        StartCoroutine("RespawnCoroutine");  //delaying the respawn
    }

    public IEnumerator RespawnCoroutine() //Unity's stupid and very complicated way to say "wait"
    {
        gamePlayer.gameObject.SetActive(false);  //turns off the player
        yield return new WaitForSeconds(respawnDelay);
        gamePlayer.transform.position = gamePlayer.RespawnPoint;  //moves the player to the respawn point
        gamePlayer.gameObject.SetActive(true);
        GameManager.Health -= 1;  //reduces health
    }
}
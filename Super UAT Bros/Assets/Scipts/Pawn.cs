﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// USed as a abstract class for others to pull and interact with 
/// </summary>

public abstract class Pawn : MonoBehaviour
{

    public float moveSpeed; // What do you think this is?
    public float jumpForce; //Jump force

    public abstract void Move( float direction );
    public abstract void Jump();

}
